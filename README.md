<h1>Trivial AP US History Web App</h1>
<p><strong>Clone and run for a quick way to start your next mobile game.</strong></p>
<p>This is a minimal Cordova application for running a website as a mobile game.</p>
<h1>To Use</h1>
<p>To clone and run this repository, you'll need To clone and run this repository you'll need <a href="https://git-scm.com" rel="nofollow noreferrer noopener" target="_blank">Git</a> and <a href="https://nodejs.org/en/download/" rel="nofollow noreferrer noopener" target="_blank">Node.js</a> (which comes with <a href="http://npmjs.com" rel="nofollow noreferrer noopener" target="_blank">npm</a>), as well as <a href="https://cordova.apache.org/">Cordova</a> installed on your computer. From your command line:</p>
<pre class="code highlight js-syntax-highlight shell white" lang="shell" v-pre="true"><code><span id="LC1" class="line" lang="shell"><span class="c"># Clone this repository</span></span>
<span id="LC2" class="line" lang="shell">git clone https://gitlab.com/agloer/trivial-apush-cordova.git</span>
<span id="LC3" class="line" lang="shell"><span class="c"># Go into the repository</span></span>
<span id="LC4" class="line" lang="shell"><span class="nb">cd </span>trivial-apush-cordova</span>
<span id="LC5" class="line" lang="shell"><span class="c"># Install platforms</span></span>
<span id="LC6" class="line" lang="shell">cordova <span class="nb">platform add ios</span></span>
<span id="LC6" class="line" lang="shell">cordova <span class="nb">platform add android</span></span>
<span id="LC7" class="line" lang="shell"><span class="c"># Run the app</span></span>
<span id="LC8" class="line" lang="shell">cordova run ios</span></code></pre>
<h1>MacInCloud instructions to generate icons and splash screen resolutions</h1>
<p>In order to best generate the icons and splash screens without having to do it manually, we'll want to edit the .bashrc to allow for install of <a href="https://github.com/ionic-team/cordova-res">Cordova-res</a>, a helper utility for generating all the different resolutions.</p>
<pre class="code highlight js-syntax-highlight shell white" lang="shell" v-pre="true"><span id="LC2" class="line" lang="shell">atom ~/.bashrc</span>
<span id="LC3" class="line" lang="shell"><span class="c"># Use your favorite text editor to edit the .bashrc.</span></span></pre>
<p>Once inside the .bashrc file, paste in the following lines:</p>
<pre><code>export ANDROID_SDK_ROOT="${HOME}/Library/Android/sdk"
export JAVA_HOME="/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home"
export GRADLE_HOME="${HOME}/gradle-6.7"
NPM_PACKAGES="${HOME}/.npm-packages"
NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
PATH="$NPM_PACKAGES/bin:$PATH:$JAVA_HOME/bin:$GRADLE_HOME/bin"
</code></pre>
<p>Now in order for this to be called every time the terminal is opened, we'll want to source it in .bash_profile.</p>
<pre class="code highlight js-syntax-highlight shell white" lang="shell" v-pre="true"><span id="LC2" class="line" lang="shell">atom ~/.bash_profile</span>
<span id="LC3" class="line" lang="shell"><span class="c"># Use your favorite text editor to edit the .bash_profile.</span></span></pre>
<p>Once inside the .bash_profile, paste in the following line:</p>
<pre><code>source "${HOME}/.bashrc"
</code>
<pre>
